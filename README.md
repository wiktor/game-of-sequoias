# Game of Sequoias

You need the target: `rustup target install wasm32-unknown-unknown`.

Get wasm-pack here: https://rustwasm.github.io/wasm-pack/installer/

And compile it: `wasm-pack build`

If it works (it won't) then go to `www` directory and use `npm install` and `npm start`.

Based on https://rustwasm.github.io/docs/book/game-of-life/hello-world.html
